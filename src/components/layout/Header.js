import React from "react";

import Button from "../commons/Button";

import AuthButton from "../auth/AuthButton";

import { Link } from "react-router-dom";

import "./Header.css";

const Header = ({ className, ...props }) => {
  return (
    <div className="layout-header">
      <nav className="nav-bar">
        <Button
          as={Link}
          to="/adverts"
          variant="primary"
          className="header-button"
        >
          Advert List
        </Button>

        <Button
          as={Link}
          to="/newAdvert"
          //to="/advert/new"
          variant="primary"
          className="header-button"
        >
          New Add
        </Button>

        <AuthButton className="header-button" />
      </nav>
    </div>
  );
};

export default Header;
