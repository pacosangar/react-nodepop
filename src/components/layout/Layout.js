import React from 'react';

import Header from './Header.js';

import './Layout.css';

const Layout = ({children, title, ...props}) =>{
    return (
        <div className="layout">
            <Header className="layout-header" { ...props} />
            <main className="layout-main">{children}</main>
            <footer className="layout-footer">2021 @pacosangar</footer>
        </div>
    );
}

export default Layout;