import React from "react";
import LoginForm from "./LoginForm";
import { login } from "../../../api/auth";
import T from "prop-types";
import { Redirect } from "react-router-dom";

import "./LoginPage.css";

function LoginPage({ onLogin }) {
  //Estado para manejar el error
  const [error, setError] = React.useState(null);

  //Estado para indicar si se esta procesando una petición a la API

  const [isLoading, setIsLoading] = React.useState(false);

  const isLogged = React.useRef(false);

  React.useEffect(() => {
    if (isLogged.current) {
      onLogin();
    }
  });

  const handleSubmit = async (credentials, rememberMe) => {
    setError(null);
    setIsLoading(true);
    try {
      await login(credentials, rememberMe);
      isLogged.current = true;
    } catch (error) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };

  if (isLogged.current) {
    return <Redirect to="/adverts" />;
  }

  return (
    <div className="loginPage">
      <h1 className="loginPage-title">Log in to Nodepop by React</h1>
      <LoginForm onSubmit={handleSubmit} isLoading={isLoading} />
      {error && <div className="loginPage-error">{error.message}</div>}
    </div>
  );
}

LoginPage.propTypes = {
  onLogin: T.func.isRequired,
};

export default LoginPage;
