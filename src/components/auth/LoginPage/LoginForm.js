import React from "react";

import Button from "../../commons/Button";

import T from "prop-types";

import FormField from "../../commons/FormField";

//import useForm from '../../../hooks/useForm';

import "./LoginForm.css";

function LoginForm ({onSubmit, isLoading}){
  //Generamos el estados que necesito.

  const [credentials, setCredentials] = React.useState({
    email: '',
    password: '',
    rememberMe: false,
  });

  const [rememberMe, setCheckBox] = React.useState(false);

  //Destructuring de credentials
  const {email, password} = credentials;

  //Creamos un manejador
  const handleChange = event =>{
    //El estado depende del anterior por lo que uso una función.
    setCredentials(oldCredentials => {
      const newCredentials = {
        ...oldCredentials,
        [event.target.name]: event.target.value ,
      }
      return newCredentials;
    });
  };

  const handleCheckBox = event =>{  
    setCheckBox(event.target.checked);
  }

  const handleSubmit = event =>{
    event.preventDefault();
    onSubmit(credentials, rememberMe);
  }


  

  return (
    <form className="loginForm" onSubmit={handleSubmit}>
      <FormField
        type="text"
        name="email"
        label="email or username"
        className="loginForm-field"
        value={email}
        onChange={handleChange}
        autoFocus
      />
      <FormField
        type="password"
        name="password"
        label="password"
        className="loginForm-field"
        value={password}
        onChange={handleChange}
      />
      <Button
        type="submit"
        className="loginForm-submit"
        variant="primary"
        disabled={isLoading || !email || !password}
      >
        Log in
      </Button>

      <FormField
        type="checkbox"
        name="rememberMe"
        label="Recuerdame"
        className="loginForm-field"
        checked={rememberMe}
        onChange={handleCheckBox}
      />    
    </form>
  );
};

LoginForm.propTypes = {
  onSubmit :T.func.isRequired,
  isLoading: T.bool,
}

LoginForm.defaultProps = {
  isLoading: false,
}

export default LoginForm;
