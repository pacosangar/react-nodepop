export { default as LoginPage } from './LoginPage/LoginPage';

export { default as PrivateRoute} from './PrivateRoute/PrivateRoute';

export { default as AuthButton } from './AuthButton/AuthButton';