import T from 'prop-types';
import { Link } from 'react-router-dom';
import { Button } from '../../commons';
import { logout } from '../../../api/auth';
import { AuthContextConsumer } from '../context';

const AuthButton = ({ className, isLogged, onLogout }) => {


  const handleLogoutClick = () =>{
      onLogout(); //pone a false el isLogged
      logout();
  }
  
  const props = isLogged
    ? { onClick: handleLogoutClick, children: 'Log out' }
    : {
        as: Link,
        to: '/login',
        children: 'Log in',
      };

  return <Button className={className} {...props} />;
};

const ConnectedAuthButton = props => {
  return (
    <AuthContextConsumer>
      {value => {
        return (
          <AuthButton
            isLogged={value.isLogged}
            onLogout={value.onLogout}
            {...props}
          />
        );
      }}
    </AuthContextConsumer>
  );
};


AuthButton.propTypes = {
  className: T.string,
  isLogged: T.bool,
  onLogout: T.func.isRequired,
};

AuthButton.defaultProps = {
  isLogged: false,
};


export default ConnectedAuthButton;
