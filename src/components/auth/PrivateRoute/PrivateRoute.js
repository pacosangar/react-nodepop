import React from "react";

import { Route, Redirect } from "react-router-dom";

import AuthContext from "../context";

const PrivateRoute = (props) => {
  const authValue = React.useContext(AuthContext);
  return authValue.isLogged ? (
    <Route {...props} />
  ) : (
    <Route>
      <Redirect to="/login" />
    </Route>
  );
};

export default PrivateRoute;
