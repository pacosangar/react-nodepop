import React from "react";
import CheckBox from "../../commons/CheckBox";

const TagList = ({tags, checked, onChange}) => {
  const arrayChecked = Object.values(checked);
  const listItems = tags.map((tag, index) => (
    <CheckBox
      key={index}
      label={tag}
      name={tag}
      value={tag}
      title={tag}
      checked={arrayChecked[index]}
      onChange= {onChange}
    />
  ));

  return <div className="tagList">{listItems}</div>;
};

export default TagList;
