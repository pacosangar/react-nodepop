import React from 'react';
import { getTags, setAdvert } from '../../../api/services';
import TagList from './TagList';


const TagsBox = (props, checked) =>{


    const [tags, setTags] = React.useState([]);

    React.useEffect(()=>{
        getTags().then(setTags);
    },[]);

    return(
        <div className='tagsBox'>
            <span>Tags</span>
            <TagList tags={tags} checked ={checked} {...props} />
        </div>
    );
}

export default TagsBox;