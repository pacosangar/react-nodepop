import React from "react";
import { saveAdvert } from "../../../api/services";

import Layout from "../../layout/Layout";
import NewAdvertForm from "./NewAdvertForm";

import { Redirect } from "react-router-dom";
import "./newAdvertPage.css";

const NewAdvertPage = (props) => {
  //Estado para manejar el error
  const [error, setError] = React.useState(null);

  //Estado para indicar si se esta procesando una petición a la API

  const [isLoading, setIsLoading] = React.useState(false);

  const isSave = React.useRef(false);
   
  const handleSubmit = async (advert) => {
    setError(null);
    setIsLoading(true);
    try {
      await saveAdvert(advert);
      isSave.current = true;
    } catch (error) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };

  if(isSave.current){
    return <Redirect to='/adverts' />
  }
  
  return (
    <div className="newAdvertPage">
      <Layout {...props}>
        <h1 className="newAdvertPage-title">Nuevo Anuncio</h1>
        <NewAdvertForm onSubmit={handleSubmit} />
        {error && <div className="loginPage-error">{error.message}</div>}
      </Layout>
    </div>
  );
};

export default NewAdvertPage;
