import React, { useState } from "react";
import { FormField, Button, RadioGroup, FormFile } from "../../commons";
import TagsBox from "../TagsBox";

import "./newAdvertPage.css";

const NewAdvertForm = ({ onSubmit, isLoading }) => {
  const [advert, setAdvert] = React.useState({
    name: "",
    price: "",
    sale: null,
    tags: [],
  });

  const [checkbox, setCheck] = React.useState({
    lifestyle: false,
    mobile: false,
    motor: false,
    work: false,
  });

  const handleCheckbox = (event) => {
    setCheck((oldCheckbox) => {
      const newCheckbox = {
        ...oldCheckbox,
        [event.target.name]: event.target.checked,
      };
      saveCheckedTags(newCheckbox);
      return newCheckbox;
    });
  };

  function saveCheckedTags(checkboxTags) {
    let tagsArray = [];
    for (const property in checkboxTags) {
      if (checkboxTags[property]) {
        tagsArray.push(property);
      }
    }
    setAdvert((oldAdvert) => {
      const newAdvert = {
        ...oldAdvert,
        tags: tagsArray,
      };
      return newAdvert;
    });
  }

  const handleChange = (event) => {
    setAdvert((oldAdvert) => {
      const newAdvert = {
        ...oldAdvert,
        [event.target.name]: event.target.value,
      };
      return newAdvert;
    });
  };

  const [radio, setRadio] = useState();

  const handleRadio = (event) => {
    setRadio(event.target.value);
    setAdvert((oldAdvert) => {
      const newAdvert = {
        ...oldAdvert,
        [event.target.name]: radio === "compra" ? true : false,
      };
      return newAdvert;
    });
  };

  const { name, price, sale, tags } = advert;

  const formfileRef = React.useRef();

  const handleSubmit = (event) => {
    event.preventDefault();

    const formData = new FormData();
    formData.append("name", advert.name);
    formData.append("price", advert.price);
    formData.append("sale", advert.sale);
    formData.append("tags", advert.tags);

    if (formfileRef.current.files.length != 0) {
      formData.append("photo", formfileRef.current.files[0]);
    }

    onSubmit(formData);
  };

  return (
    <div className="newAdvertPage-form">
      <form onSubmit={handleSubmit}>
        <FormField
          type="text"
          name="name"
          label="Producto"
          className="loginForm-field"
          value={name}
          onChange={handleChange}
          autoFocus
        />
        <FormField
          type="number"
          name="price"
          label="Precio"
          className="loginForm-field"
          value={price}
          onChange={handleChange}
        />
        <TagsBox onChange={handleCheckbox} checked={checkbox} />
        <RadioGroup onChange={handleRadio} radio={radio} />

        <FormFile label="imagen" name="photo" inputRef={formfileRef} />

        <Button
          type="submit"
          className="loginForm-submit"
          variant="primary"
          disabled={
            isLoading || !name || !price || sale === null || tags.length === 0
          }
        >
          Publicar Anuncio
        </Button>
      </form>
    </div>
  );
};

export default NewAdvertForm;
