import React from "react";

import { getAdvert, deleteAdvert } from "../../../api/services";
import Layout from "../../layout/Layout";
import { Redirect } from "react-router-dom";
import "../AdverPage/gridbox.css";
import {Button} from "../../commons"

const AdvertPageDetail = ({ match, history, ...props }) => {
  //const id = match.params.id;

  const [advert, setAdvert] = React.useState([]);

 

  React.useEffect(() => {
    getAdvert(match.params.id)
      .then(setAdvert)
      .catch((error) => {
        setAdvert(error.statusCode);
      });
  }, []);

  if (advert === 404) {
    return <Redirect to="/404" />;
  }

  const handleDelete = async event =>{
    try{
      console.log('Eliminar ID', match.params.id);
      if(window.confirm('¿Seguro que quieres BORRAR el anuncio')){
        await deleteAdvert(match.params.id);
        history.push('/');
      }
    }catch(error){
      window.alert(error);
    }
  }
 
  const item = (
    <article key={advert.id} className="grid-item">
      <div className="adbody">
        <img
          src={
              advert.photo ? (
                `${process.env.REACT_APP_API_BASE_URL}` + advert.photo
              )
              : ('/default.jpg')
          }
          alt={advert.photo ? advert.name : 'Imagen por defecto'}
        />
        <div className="info">
          <h3>{advert.price} €</h3>
          <p>{advert.name}</p>
          <p>{advert.tags}</p>
          <p>{advert.sale ? <span>Venta</span> : <span>Compra</span>}</p>
        </div>
      </div>

      <div className="nofloat"></div>
      <Button 
        className="loginForm-submit"
        variant="primary"
        onClick={handleDelete}
      >
        Eliminar
      </Button>
     
    </article>
  );

  //Renderizado
  return (
    <Layout {...props}>
      <div className="AdvertPageDetail, grid-content-ads">{item}</div>
    </Layout>
  );
};

export default AdvertPageDetail;
