import React from "react";

import {getLastestAdverts} from '../../../api/services';
import Button from "../../commons/Button";
import Layout from "../../layout/Layout";
import AdvertList from "./AdvertList";

import {Link} from "react-router-dom";

import './gridbox.css';


const EmptyList = () =>(
    <div style={{textAlign: 'center', padding: '2em'}}>
        <p style={{padding: '1em'}}>Inserta tú primer anuncio</p>
        <Button
            as={Link}
            to="/advert/new"
            variant="primary"
        >
            New Advert
        </Button>
    </div>
);




export const AdvertsPage = ({ className, ...props }) => {

    const [adverts, setAdverts] = React.useState([]);

    React.useEffect(()=>{
        getLastestAdverts().then(setAdverts);
    }, []);
 
 
    //Renderizado
    return (
        <Layout { ...props}>
            <div className="advertsPage">
                {adverts.length ? <AdvertList adverts={adverts} /> : <EmptyList />}
            </div>
        </Layout>   
    ); 
};

export default AdvertsPage;
