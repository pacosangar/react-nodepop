import React from "react";

import { Link } from "react-router-dom";

const AdvertList = ({ adverts, ...props }) => {
  const items = adverts.map((ad) => (
    <article key={ad.id} className="grid-item">
      <Link to={`/advert/${ad.id}`}>
        <div className="adbody">
          <div className="info">
            <h3>{ad.price} €</h3>
            <p>{ad.name}</p>
            <p>{ad.tags}</p>
            <p>{ad.sale ? <span>Venta</span> : <span>Compra</span>}</p>
          </div>
        </div>
        <div className="nofloat"></div>
      </Link>
    </article>
  ));

  return <div className="advertList, , grid-content-ads">{items}</div>;
};

export default AdvertList;
