import React from 'react';

import T from "prop-types";

import RadioButton from './RadioButton';

import './radiogroup.css';

function RadioGroup({ label, ...props }) {

  return (
    <div>
      <label className="radioGroup-label">
        <span>{label}</span>
        <br/>
        <div className="radioGroup-imput">
        <RadioButton 
            name="sale"
            value="venta"
            title="Venta"
            {...props} 
        />
        <RadioButton 
            name="sale"
            value="compra"
            title="Compra"
            {...props} 
        />
        </div>
        <br/>
      </label>
    </div>
  );
}

RadioGroup.propTypes = {
//   className: T.string.isRequired,
  label: T.string,
}


export default RadioGroup;