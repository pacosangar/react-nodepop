
import React from 'react';



const RadioButton = ({title = "", name, value, onChange, radio}) =>{

    return (
    <label>
        <input
            type="radio"
            name={name}
            value={value}
            checked ={radio === value}
            onChange ={onChange}
        />
        {" " + title}
    </label>
    );
};

export default RadioButton;