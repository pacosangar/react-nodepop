import React from 'react';

import classNames from 'classnames';

import T from "prop-types";

import './FormField.css';

function FormField({ className, label, autoFocus, ...props }) {
  const inputRef = React.useRef(null);

  React.useEffect(() => {
    if(autoFocus){
      inputRef.current.focus();
    }
  }, []);
 
  

  return (
    <div
      className={classNames(
        'formField',
        { 'formField--focused': false },
        className
      )}
    >
      <label className="formField-label">
        <span>{label}</span>
        <input
          ref={inputRef}
          className="formField-input"
          autoComplete="off"
          {...props}
        />
      </label>
    </div>
  );
}

FormField.propTypes = {
  className: T.string.isRequired,
  label: T.string.isRequired,
}


export default FormField;
