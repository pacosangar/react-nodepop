// import { func } from 'prop-types';
import React from "react";

const CheckBox = ({ title = "", name, value, checked, onChange }) => {
 
  return (
    <label>
      <input
        type="checkbox"
        name={name}
        value={value}
        checked = {checked}
        onChange={onChange}
      />
      {" " + title}
    </label>
  );
};

export default CheckBox;
