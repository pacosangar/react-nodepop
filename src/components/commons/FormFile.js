import React from 'react';




const FormFile = ({label, inputRef, ...props}) => {
    

    return(
        <label>
            <span>{label}</span>
            <div className="fromFile">
                <input 
                    type ='file'
                    ref={inputRef} 
                    {...props}
                />
            </div>
        </label>
    );
}

export default FormFile;