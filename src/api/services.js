import client from './client.js';

const serverBaseURL = '/api/v1';

export const getLastestAdverts = () =>{
    const url = `${serverBaseURL}/adverts`;
    return client.get(url);
}

export const getAdvert = (id) =>{
    const url = `${serverBaseURL}/adverts/${id}`;
    return client.get(url);
}

export const saveAdvert = advertData => { 
    const url = `${serverBaseURL}/adverts`;
    return client.post(url, advertData);
}

export const getTags = tags => {
    const url = `${serverBaseURL}/adverts/tags`;
    return client.get(url);
}

export const deleteAdvert = (id) =>{
    const url = `${serverBaseURL}/adverts/${id}`;
    return client.delete(url);
}