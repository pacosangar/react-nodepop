import React from "react";

import "./App.css";

import { AdvertsPage, AdvertPageDetail, NewAdvertPage } from "./components/adverts";

import { Switch, Route, Redirect } from "react-router-dom";

import { LoginPage, PrivateRoute } from "./components/auth";

import { AuthContextProvider } from "./components/auth/context";

function App({ isInitiallyLogged }) {
  const [isLogged, setIsLogged] = React.useState(isInitiallyLogged);

  const handleLogin = () => setIsLogged(true);

  const handleLogOut = () => setIsLogged(false);

  const authValue = {
    isLogged,
    onLogout: handleLogOut,
    onLogin: handleLogin,
  };


  return (
    <div className="App">
      <AuthContextProvider value={authValue}>
        <Switch>
          <Route path="/login">
            <LoginPage onLogin={handleLogin} />
          </Route>

          <PrivateRoute exact path="/">
            <Redirect to="/adverts" />
          </PrivateRoute>

          <PrivateRoute path="/adverts">
            <AdvertsPage />
          </PrivateRoute>

          <PrivateRoute path="/advert/:id">
            {({ match, history }) => <AdvertPageDetail match={match} history={history} />}
          </PrivateRoute>

          <PrivateRoute path="/newAdvert">
            <NewAdvertPage />
          </PrivateRoute>

          <Route path="/404">
            <div
              style={{
                textAlign: "center",
                fontSize: 48,
                fontWeight: "bold",
              }}
            >
              404 | Not found page
            </div>
          </Route>

          <Route>
            <Redirect to="/404" />
          </Route>
        </Switch>
      </AuthContextProvider>
    </div>
  );
}

export default App;
